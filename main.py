import yaml
from yaml.loader import SafeLoader
import requests
import asyncio
import aioping
import sched, time
import schedule
import threading
import logging as log
from datetime import datetime, timedelta
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

class Influx:
    def __init__(self, bucket, org, url, token, interval):
        self.bucket = bucket
        self.org = org
        self.url = url
        self.token = token
        self.interval = interval

        self.client = InfluxDBClient(url=self.url, token=self.token)
        self.write_api = self.client.write_api(write_options=SYNCHRONOUS)

    def write(self, name, field_name, value, group):
        try:
            point = Point(name)\
                .field(field_name, value)\
                .tag('group', group)\
                .time(datetime.utcnow(), WritePrecision.NS)
            result = self.write_api.write(self.bucket, self.org, point)
            return(result)
        except Exception as e:
            log.error(f"influx Write Exception {e}")
            return e

    def queryLast(self, measurement):
        try:
            query = f"""from(bucket: "{self.bucket}")
                |> range(start: -{self.interval + 120}s, stop: now())
                |> filter(fn: (r) => r["_measurement"] == "{measurement}")
                |> filter(fn: (r) => r["_field"] == "status_code")
                |> yield(name: "last")"""
            tables = self.client.query_api().query(query, org=self.org)
            results = []
            for table in tables:
                for record in table.records:
                    results.append((record.get_value(), record.get_field()))
            if len(results) > 0:
                result = str(results[len(results)-1][0])
            else:
                result = "INIT"
            log.debug(f"Query Last for {measurement} returned: {result}")
            return result
        except Exception as e:
            log.error(f"Influx Query Exception: {e}")
            return "INIT"

    def writeStatusCode(self, name, statusCode, group=None):
        self.writeResponse = influxdb.write(name = name, field_name = "status_code", value=str(statusCode), group=group)

    def writeStatusUP(self, name, group=None):
        self.writeResponse = influxdb.write(name = name, field_name = "status_code", value=str("OK"), group=group)

    def writeStatusDOWN(self, name, group=None):
        self.writeResponse = influxdb.write(name = name, field_name = "status_code", value=str("DOWN"), group=group)

    def writeLatency(self, name, delay, group=None):
        if delay is None:
            delay = 0
        influxdb.writeResponse = influxdb.write(name = name, field_name = "latency", value=float(delay), group=group)
class Telegram:
    def __init__(self, telegram_token, telegram_chatID):
        self.bot_token = telegram_token
        self.bot_chatID = telegram_chatID

    def sendMessage(self, message):
        send_text = f"https://api.telegram.org/bot{self.bot_token}/sendMessage?chat_id={self.bot_chatID}&parse_mode=Markdown&text={message}"
        response = requests.get(send_text)
        log.debug(f"Telegram Response: {response.json()}")
        return response.json()

    def sendUP(self, name):
        self.sendMessage(message = f"🟢 \"{name}\" is UP ✅")

    def sendDOWN(self, name):
        self.sendMessage(message = f"🔴 \"{name}\" is DOWN ❌")

# Open the file and load the file
def openConfigYaml(fileName):
    with open(fileName) as conf:
        data = yaml.load(conf, Loader=SafeLoader)
        return data
class httpHost:
    def __init__(self, host, defaults):
        self.name = host.get('name', None)
        self.url = host.get('url', None)
        self.group = host.get('group', None)
        self.maxDownCount = host.get('maxDownCount', defaults.get('defaultMaxDownCount'))

        self.latestStatusCode = None
        self.currentDownCount = 0

        self.previousState = influxdb.queryLast(measurement=self.name)
        self.lastDBState = None

    def check(self):
        try:
            response = requests.get(url=self.url)
            responseStatusCode = response.status_code
            if responseStatusCode == 200:
                result = "OK"
            else:
                result = "FAILED"
        except Exception as e:
            log.debug(f"HTTP: Exception {e}")
            result = "FAILED"
        try:
            if (result is "OK"):
                log.debug(f"🟢 HTTP: \"{self.name}\" is UP ✅")
                self.latestStatusCode = "OK"
                self.send()
                self.previousState = "OK"

            elif result is "FAILED": #when Host not Reached
                if self.latestStatusCode == "DOWN": #when already down
                    self.send()
                    self.previousState = "DOWN"
                else:
                    log.debug(f"🟠 PING: \"{self.name}\" is PENDING")
                    self.latestLatency = None
                    self.latestStatusCode = "PENDING"
                    self.send()
                    self.previousState = "PENDING"


        except Exception as e:
            log.error(f"Processing HTTP Result Failed! Exception: {e}")
            result = False
        return result

    def write(self):
        influxdb.writeStatusCode(name=self.name, statusCode = self.latestStatusCode, group=self.group)

    def send(self):
        if self.latestStatusCode == "OK" and self.previousState != "OK":
            telegram.sendMessage(message = f"🟢 \"{self.name}\" is UP ✅")

        elif self.latestStatusCode == "PENDING":
            if self.currentDownCount == self.maxDownCount: # when maximum DownCount reached
                self.currentDownCount = 0
                self.latestStatusCode = "DOWN"
            else:
                log.debug(f"🟠 \"{self.name}\" is Pending: {self.currentDownCount} of {self.maxDownCount}")
                #telegram.sendMessage(message = f"🟠 \"{self.name}\" is Pending: {self.currentDownCount} of {self.maxDownCount}")
                self.currentDownCount += 1

        elif self.latestStatusCode == "DOWN" and self.previousState != "DOWN":
            telegram.sendMessage(message = f"🔴 \"{self.name}\" is DOWN ❌")
            self.latestStatusCode = "DOWN"


class pingHost:
    def __init__(self, pingHost, defaults):
        self.name = pingHost.get('name', None)
        self.ip = pingHost.get('ip', None)
        self.group = pingHost.get('group', None)
        self.maxDownCount = host.get('maxDownCount', defaults.get('defaultMaxDownCount'))
        self.timeout = pingHost.get('timeout', defaults.get('defaultPingTimeout'))

        self.latestLatency = None
        self.latestStatusCode = None
        self.currentDownCount = 0

        self.previousState = influxdb.queryLast(measurement=self.name)
        self.lastDBState = None

    async def check(self):
        log.debug(f"Ping: Name: {self.name} IP: {self.ip}")
        try:
            delay = await aioping.ping(self.ip, timeout = self.timeout) * 1000
            result = "OK"
        except TimeoutError:
            result = "timeout"
        except:
            result = "notFound"
        try:
            if result is "OK":
                log.debug(f"🟢 PING: \"{self.name}\" is UP ✅")
                self.latestLatency = delay
                self.latestStatusCode = "OK"
                self.send()
                self.previousState = "OK"

            elif result is "timeout" or result is "notFound": #when Host not Reached
                if self.latestStatusCode == "DOWN": #when already down
                    self.send()
                    self.previousState = "DOWN"
                else:
                    log.debug(f"🟠 PING: \"{self.name}\" is PENDING")
                    self.latestLatency = None
                    self.latestStatusCode = "PENDING"
                    self.send()
                    self.previousState = "PENDING"

        except Exception as e:
            log.error(f"Processing Ping Exception: {e}")
            result = False
        return result

    def write(self):
        influxdb.writeLatency(name=self.name, delay=self.latestLatency, group=self.group)
        influxdb.writeStatusCode(name=self.name, statusCode = self.latestStatusCode, group=self.group)

    def send(self):
        if self.latestStatusCode == "OK" and self.previousState != "OK":
            telegram.sendMessage(message = f"🟢 \"{self.name}\" is UP ✅")

        elif self.latestStatusCode == "PENDING":
            if self.currentDownCount == self.maxDownCount: # when maximum DownCount reached
                self.currentDownCount = 0
                self.latestStatusCode = "DOWN"
            else:
                log.debug(f"🟠 \"{self.name}\" is Pending: {self.currentDownCount} of {self.maxDownCount}")
                #telegram.sendMessage(message = f"🟠 \"{self.name}\" is Pending: {self.currentDownCount} of {self.maxDownCount}")
                self.currentDownCount += 1

        elif self.latestStatusCode == "DOWN" and self.previousState != "DOWN":
            telegram.sendMessage(message = f"🔴 \"{self.name}\" is DOWN ❌")
            self.latestStatusCode = "DOWN"

def writeAll(pingHosts = None, httpHosts = None):
    if pingHosts:
        for pingHost in pingHosts:
            pingHost.write()

    if httpHosts:
        for httpHost in httpHosts:
            httpHost.write()

def main(pingHosts = None, httpHosts = None):
    if pingHosts:
        for pingHost in pingHosts:
            loop.run_until_complete(pingHost.check())

    if httpHosts:
        for httpHost in httpHosts:
            httpHost.check()
            

if __name__ == '__main__':

    loop = asyncio.get_event_loop()
    configData = openConfigYaml(fileName='config.yaml')

    INTERVAL = configData['config']['interval']
    debugLevel = configData['config']['debug']
    
    defaults = configData.get('config', None)

    influxDB_token = configData['influxdb']['token']
    influxDB_org = configData['influxdb']['org']
    influxDB_url = configData['influxdb']['url']
    influxDB_bucket = configData['influxdb']['bucket']
    influxDB_interval = configData['influxdb']['interval']

    telegram_token = configData['telegram']['token']
    telegram_chatID = configData['telegram']['chatid']

    telegram = Telegram(telegram_token=telegram_token, telegram_chatID=telegram_chatID)
    influxdb = Influx(bucket=influxDB_bucket, org=influxDB_org, url=influxDB_url, token=influxDB_token, interval=INTERVAL)

    #if debugLevel is True:
    #    log.basicConfig(level=log.DEBUG)
    #else:
    #    log.basicConfig(level=log.INFO)

    log.basicConfig(level=log.DEBUG)

    pingHostsList = []
    for host in configData['ping']:
        pingHostsList.append(pingHost(host, defaults))

    httpHostsList = []
    for host in configData['http']:
        httpHostsList.append(httpHost(host, defaults)) 

    schedule.every(INTERVAL).seconds.do(main, pingHosts=pingHostsList, httpHosts=httpHostsList)
    schedule.every(influxDB_interval).seconds.do(writeAll, pingHosts=pingHostsList, httpHosts=httpHostsList)

    main(pingHostsList, httpHostsList)
    writeAll(pingHostsList, httpHostsList)

    while True and debugLevel is False:
        schedule.run_pending()
        time.sleep(1)

