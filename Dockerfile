FROM python:3

ADD main.py /
ADD requirements.txt /
ADD config.yaml /

RUN pip install -r requirements.txt


CMD [ "python", "./main.py" ]